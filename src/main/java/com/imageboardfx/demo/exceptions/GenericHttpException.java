package com.imageboardfx.demo.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class GenericHttpException extends RuntimeException {
    public GenericHttpException(String message) {
        super(message);
    }
}
