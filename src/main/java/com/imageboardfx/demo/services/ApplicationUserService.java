package com.imageboardfx.demo.services;

import com.imageboardfx.demo.dao.user.ApplicationUserDAO;
import com.imageboardfx.demo.pojo.ApplicationUser;
import com.imageboardfx.demo.pojo.ConfirmUserRegistration;
import com.imageboardfx.demo.pojo.RequestResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class ApplicationUserService {

    private final ApplicationUserDAO applicationUserDAO;

    @Autowired
    public ApplicationUserService(@Qualifier("postgresApplicationUser") ApplicationUserDAO applicationUserDAO) {
        this.applicationUserDAO = applicationUserDAO;
    }

    public RequestResult confirmRegistration(ConfirmUserRegistration confirmUserRegistration) {
        return this.applicationUserDAO.confirmRegistration(confirmUserRegistration);
    }

    public RequestResult register(String email, String username, String password) {
        return this.applicationUserDAO.registerUser(email, username, password);
    }

    public RequestResult login(String email, String password, String jwtToken) {
        return this.applicationUserDAO.login(email, password, jwtToken);
    }

    public RequestResult changePSWD(String emailCode, String email, String newPassword) {
        return this.applicationUserDAO.changePSWD(emailCode, email, newPassword);
    }

    public RequestResult deleteUser(String email, String password) {
        return this.applicationUserDAO.deleteUser(email, password);
    }

    public RequestResult sendCodeToEmail(String email) {
        return this.applicationUserDAO.sendCodeToEmail(email);
    }
}
