package com.imageboardfx.demo.services;

import com.imageboardfx.demo.dao.posts.PostDAO;
import com.imageboardfx.demo.pojo.Post;
import com.imageboardfx.demo.pojo.RequestResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class PostService {

    private final PostDAO postDAO;

    @Autowired
    public PostService(@Qualifier("postgres-post") PostDAO postDAO) {
        this.postDAO = postDAO;
    }

    public RequestResult newPost(Post newPost) { return this.postDAO.newPost(newPost); }

    public RequestResult updatePost(long id, Post newPost) {
        return this.postDAO.updatePost(id, newPost);
    }

    public RequestResult getPosts(long startIndex, long limit) {
        return this.postDAO.getPosts(startIndex, limit);
    }

    public RequestResult getPost(long id) { return this.postDAO.getPost(id); }

    public RequestResult getNumberOfPost() { return this.postDAO.getNumberOfPost(); }

    public RequestResult deletePost(long id) {
        return this.postDAO.deletePost(id);
    }
}
