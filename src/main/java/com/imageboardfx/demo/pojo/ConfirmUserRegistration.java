package com.imageboardfx.demo.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;

public class ConfirmUserRegistration {

    @NotBlank
    private final String email;

    @NotBlank
    private final String emailCode;

    public ConfirmUserRegistration(@JsonProperty("email") String email,
                                   @JsonProperty("email-code") String emailCode) {
        this.email = email;
        this.emailCode = emailCode;
    }

    public String getEmail() {
        return email;
    }

    public String getEmailCode() {
        return emailCode;
    }
}
