package com.imageboardfx.demo.pojo;

import org.springframework.http.HttpStatus;

/**
 * Used for responding back to client
 */
public class RequestResult {

    private final HttpStatus status;
    private String message;
    private Object respObject;

    public RequestResult(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
    }

    public RequestResult(HttpStatus status, Object respObject) {
        this.status = status;
        this.respObject = respObject;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public Object getRespObject() {
        return respObject;
    }
}
