package com.imageboardfx.demo.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Date;
import java.util.Set;

import static com.imageboardfx.demo.security.ApplicationUserRole.USER;

public class ApplicationUser {

    private String email;
    private String password;
    private String username;
    private String jwtKey;
    private boolean isAccountNonExpired;
    private boolean isAccountNonLocked;
    private boolean isCredentialsNonExpired;
    private boolean isEnabled;
    private Set<? extends GrantedAuthority> grantedAuthorities;
    private Date accCreationDate;

    private Date lastAddedPost;

    public ApplicationUser() {
    }

    public ApplicationUser(
            String email,
            String username,
            String password,
            boolean isAccountNonExpired,
            boolean isAccountNonLocked,
            boolean isCredentialsNonExpired,
            boolean isEnabled,
            Set<? extends GrantedAuthority> grantedAuthorities,
            Date lastAddedPost,
            Date accCreationDate) {
        this.email = email;
        this.password = password;
        this.username = username;
        this.jwtKey = null;
        this.isAccountNonExpired = isAccountNonExpired;
        this.isAccountNonLocked = isAccountNonLocked;
        this.isCredentialsNonExpired = isCredentialsNonExpired;
        this.isEnabled = isEnabled;
        this.grantedAuthorities = grantedAuthorities;
        this.lastAddedPost = lastAddedPost;
        this.accCreationDate = accCreationDate;
    }

    public ApplicationUser(@JsonProperty("email") String email,
                           @JsonProperty("password") String password) {
        this.email = email;
        this.password = password;
        this.jwtKey = null;
        this.username = "";
        this.isAccountNonExpired = true;
        this.isAccountNonLocked = true;
        this.isCredentialsNonExpired = true;
        this.isEnabled = true;
        this.grantedAuthorities = USER.getGrantedAuthorities();
        this.lastAddedPost = null;
        this.accCreationDate = null;
    }

    public ApplicationUser(@JsonProperty("jwtKey") String jwtKey) {
        this.email = "";
        this.password = "";
        this.username = "";
        this.jwtKey = jwtKey;
        this.isAccountNonExpired = true;
        this.isAccountNonLocked = true;
        this.isCredentialsNonExpired = true;
        this.isEnabled = true;
        this.grantedAuthorities = USER.getGrantedAuthorities();
        this.lastAddedPost = null;
        this.accCreationDate = null;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public String getEmail() {
        return this.email;
    }

    public void setLastAddedPost(Date newDate) {
        this.lastAddedPost = newDate;
    }

    public Date getLastAddedPost() {
        return this.lastAddedPost;
    }

    public Date getAccCreationDate() {
        return this.accCreationDate;
    }

    public void setPassword(String newPassword) {
        this.password = newPassword;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.grantedAuthorities;
    }

    public String getPassword() {
        return this.password;
    }

    public void setUsername(String newUsername) {
        this.username = newUsername;
    }

    public String getNick() {
        return this.username;
    }

    public String getUsername() {
        return this.username;
    }

    public String getJwtKey() {
        return this.jwtKey;
    }

    public boolean isAccountNonExpired() {
        return this.isAccountNonExpired;
    }

    public boolean isAccountNonLocked() {
        return this.isAccountNonLocked;
    }

    public boolean isCredentialsNonExpired() {
        return this.isCredentialsNonExpired;
    }

    public boolean isEnabled() {
        return this.isEnabled;
    }
}
