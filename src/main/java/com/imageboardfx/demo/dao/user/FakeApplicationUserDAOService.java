package com.imageboardfx.demo.dao.user;

import com.imageboardfx.demo.pojo.ApplicationUser;
import com.imageboardfx.demo.pojo.ConfirmUserRegistration;
import com.imageboardfx.demo.pojo.RequestResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.imageboardfx.demo.security.ApplicationUserRole.ADMIN;
import static com.imageboardfx.demo.security.ApplicationUserRole.USER;

@Repository("fakeApplicationUser")
public class FakeApplicationUserDAOService implements ApplicationUserDAO {

    private final PasswordEncoder passwordEncoder;

    /**
     * Holds all registered users.
     */
    private List<ApplicationUser> applicationUsers;

    @Autowired
    public FakeApplicationUserDAOService(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
        this.applicationUsers = new ArrayList<>();
        this.applicationUsers.add(
                new ApplicationUser(
                        "anna@gmail.com",
                        "anna",
                        passwordEncoder.encode("password"),
                        true,
                        true,
                        true,
                        true,
                        USER.getGrantedAuthorities(),
                        Date.from(Instant.now()),
                        Date.from(Instant.now())
                ));

        applicationUsers.add(
                new ApplicationUser(
                        "tom@gmail.com",
                        "tom",
                        passwordEncoder.encode("pass"),
                        true,
                        true,
                        true,
                        true,
                        ADMIN.getGrantedAuthorities(),
                        Date.from(Instant.now()),
                        Date.from(Instant.now())
                ));

        applicationUsers.add(
                new ApplicationUser(
                        "vlad123@gmail.com",
                        "vlad",
                        passwordEncoder.encode("password123"),
                        true,
                        true,
                        true,
                        true,
                        USER.getGrantedAuthorities(),
                        Date.from(Instant.now()),
                        Date.from(Instant.now())
                ));
    }

    @Override
    public RequestResult confirmRegistration(ConfirmUserRegistration confirmUserRegistration) {
        //TODO:
        return new RequestResult(HttpStatus.OK, SUCCESS_MSG);
    }

    @Override
    public RequestResult registerUser(String email, String username, String password) {
        if (this.hasUser(username)) {
            return new RequestResult(HttpStatus.OK, FAILURE_MSG);
        }
        // Here we change the plain text password to the encoded one
        ApplicationUser newUser = new ApplicationUser(email, this.passwordEncoder.encode(password));
        newUser.setUsername(username);
        this.applicationUsers.add(newUser);
        return new RequestResult(HttpStatus.OK, SUCCESS_MSG);
    }

    @Override
    public Optional<ApplicationUser> getUserByUsername(String username) {
        return this.applicationUsers
                .stream()
                .filter(applicationUser -> applicationUser.getUsername().equals(username))
                .findFirst();
    }

    @Override
    public Optional<ApplicationUser> getUserByEmail(String email) {
        return this.applicationUsers
                .stream()
                .filter(applicationUsers -> applicationUsers.getEmail().equals(email))
                .findFirst();
    }

    @Override
    public boolean hasEmail(String email) {
        return this.applicationUsers
                .stream()
                .anyMatch(u -> u.getEmail().equals(email));
    }

    @Override
    public boolean hasUser(String username) {
        return this.applicationUsers
                .stream()
                .anyMatch(u -> u.getUsername().equals(username));
    }

    @Override
    public RequestResult login(String email, String password, String jwtToken) {
        final boolean logged = this.applicationUsers
                .stream()
                .anyMatch(u -> u.getPassword().equals(password) && u.getEmail().equals(email));
        if (logged) {
            return new RequestResult(HttpStatus.OK, SUCCESS_MSG);
        }
        return new RequestResult(HttpStatus.BAD_REQUEST, FAILURE_INVALID_USERNAME_OR_PASSWORD);
    }

    @Override
    public RequestResult changePSWD(String email, String password, String newPassword) {
//        if (!this.hasRegisteredUser(email, password)) {
//            return false;
//        }
        final String encodedPassword = this.passwordEncoder.encode(newPassword);
        this.applicationUsers = this.applicationUsers
                .stream()
                .peek(user -> user.setPassword(encodedPassword)).collect(Collectors.toList());
        return new RequestResult(HttpStatus.OK, SUCCESS_MSG);
    }

    @Override
    public RequestResult deleteUser(String username, String password) {
        final RequestResult hasLogin = this.login(username, password, null);
        if (hasLogin.getStatus() != HttpStatus.OK) {
            return new RequestResult(HttpStatus.NOT_ACCEPTABLE, FAILURE_INVALID_USERNAME_OR_PASSWORD);
        }
        final Optional<ApplicationUser> user = this.getUserByEmail(username);
        if (user.isPresent()) {
            this.applicationUsers.remove(user.get());
            return new RequestResult(HttpStatus.OK, SUCCESS_MSG);
        }
        return new RequestResult(HttpStatus.INTERNAL_SERVER_ERROR, FAILURE_INTERNAL_MSG);
    }

    @Override
    public RequestResult sendCodeToEmail(String email) {
        return new RequestResult(HttpStatus.OK, SUCCESS_MSG);
    }
}
