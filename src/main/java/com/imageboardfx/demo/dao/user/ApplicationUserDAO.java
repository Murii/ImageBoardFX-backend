package com.imageboardfx.demo.dao.user;

import com.imageboardfx.demo.pojo.ApplicationUser;
import com.imageboardfx.demo.pojo.ConfirmUserRegistration;
import com.imageboardfx.demo.pojo.RequestResult;

import java.util.Optional;

public interface ApplicationUserDAO {

    String SUCCESS_MSG = "{\"success\":1}";
    String FAILURE_MSG = "{\"success\":0}";
    String FAILURE_EXPIRED_JWT = "{\"success\":0, \"msg\":\"Expired jwt\"}";
    String FAILURE_INVALID_JWT = "{\"success\":0, \"msg\":\"Invalid jwt\"}";
    String FAILURE_INVALID_USERNAME_OR_PASSWORD = "{\"success\":0, \"msg\":\"Invalid username or password\"}";
    String FAILURE_EMAIL_CODE_NOT_VALIDATED = "{\"success\":0, \"msg\":\"Email code not validated\"}";
    String FAILURE_USER_ALREADY_EXISTS = "{\"success\":0, \"msg\":\"User already exists\"}";
    String FAILURE_INTERNAL_MSG = "{\"success\":0, \"msg\":\"Internal server error\"}";
    String FAILURE_INVALID_EMAIL_FORMAT_MSG = "{\"success\":0, \"msg\":\"Invalid email format\"}";
    String FAILURE_INVALID_EMAIL_MSG = "{\"success\":0, \"msg\":\"Invalid email\"}";
    String FAILURE_INCORRECT_EMAIL_CODE = "{\"success\":0, \"msg\":\"Invalid email code\"}";
    String FAILURE_EXPIRED_RESET_PASSWORD_CODE = "{\"success\":0, \"msg\":\"Reset password code has expired or it's invalid\"}";

    RequestResult confirmRegistration(ConfirmUserRegistration confirmUserRegistration);
    RequestResult login(String email, String password, String jwtToken);
    RequestResult changePSWD(String emailCode, String email, String newPassword);
    RequestResult registerUser(String email, String username, String password);
    RequestResult deleteUser(String username, String password);
    RequestResult sendCodeToEmail(String email);
    Optional<ApplicationUser> getUserByUsername(String username);
    Optional<ApplicationUser> getUserByEmail(String email);
    boolean hasEmail(String email);
    boolean hasUser(String username);
}
