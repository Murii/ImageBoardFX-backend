package com.imageboardfx.demo.dao.user;

import com.imageboardfx.demo.pojo.ApplicationUser;
import com.imageboardfx.demo.pojo.ConfirmUserRegistration;
import com.imageboardfx.demo.pojo.RequestResult;
import com.imageboardfx.demo.security.EmailUniqueCode;
import com.imageboardfx.demo.services.EmailServiceImpl;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import static com.imageboardfx.demo.security.ApplicationUserRole.USER;

@Repository("postgresApplicationUser")
public class ApplicationUserDAOService implements ApplicationUserDAO {
    private final JdbcTemplate jdbcTemplate;
    private final PasswordEncoder passwordEncoder;

    @Value("${mail.codeExpiresIn}")
    private int codeExpiresIn;

    @Value("${jwt.secret-key}")
    private String jwtKey;

    @Value("${jwt.expire-in}")
    private long jwtKeyExpireIn;

    private final Logger logger = Logger.getLogger(ApplicationUserDAOService.class.getName());

    @Autowired
    private EmailServiceImpl emailService;

    @Autowired
    public ApplicationUserDAOService(JdbcTemplate jdbcTemplate, PasswordEncoder passwordEncoder) {
        this.jdbcTemplate = jdbcTemplate;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public boolean hasUser(String username) {
        final String sql = "SELECT username FROM user_table WHERE username = ?";
        List<Object> result = this.jdbcTemplate.query(sql, new Object[]{username}, (rs, rowNum) ->
                !rs.getString("username").isEmpty());
        return !result.isEmpty();
    }

    @Override
    public boolean hasEmail(String email) {
        final String sql = "SELECT email FROM user_table WHERE email = ?";
        List<Object> result = this.jdbcTemplate.query(sql, new Object[]{email}, (rs, rowNum)
                -> !rs.getString("email").isEmpty());
        return !result.isEmpty();
    }

    @Override
    public RequestResult login(String email, String password, String jwtToken) {
        /*
         * If we get from client a jwt token validate it and
         * if it passes login the user.
         */
        if (jwtToken != null) {
            try {
                Claims claims = Jwts.parser().setSigningKey(jwtKey).parseClaimsJws(jwtToken).getBody();
                if (claims.getExpiration().before(new Date())) {
                    return new RequestResult(HttpStatus.BAD_REQUEST, FAILURE_EXPIRED_JWT);
                }
                return new RequestResult(HttpStatus.OK, SUCCESS_MSG);
            } catch (SignatureException e) {
                return new RequestResult(HttpStatus.BAD_REQUEST, FAILURE_INVALID_JWT);
            }
        }
        /* Since we use the same method for validating an email when registering we know this is safe.
         * Check if the entered email is valid. We don't have to waste CPU cycles on invalid input email data. */
        if (this.isEmailInvalid(email)) {
            this.logger.warning("Cannot login, invalid email format: " + email);
            return new RequestResult(HttpStatus.BAD_REQUEST, FAILURE_INVALID_EMAIL_FORMAT_MSG);
        } else if (this.checkEmailIsBlocked(email)) {
            this.logger.warning("Email: " + email + " did not validate his account");
            return new RequestResult(HttpStatus.BAD_REQUEST, FAILURE_EMAIL_CODE_NOT_VALIDATED);
        }
        final String sql = "SELECT email, password, username FROM user_table WHERE email = ?";
        ApplicationUser applicationUser;
        try {
            applicationUser = this.jdbcTemplate.queryForObject(sql, new Object[]{email}, (rs, rowNum) -> {
                ApplicationUser r = new ApplicationUser();
                r.setUsername(rs.getString("username"));
                r.setPassword(rs.getString("password"));
                r.setEmail(rs.getString("email"));
                return r;
            });
        } catch (Exception e) {
            return new RequestResult(HttpStatus.BAD_REQUEST, FAILURE_INVALID_USERNAME_OR_PASSWORD);
        }
        final boolean success =
                applicationUser != null
                        && applicationUser.getEmail().equals(email)
                        && this.passwordEncoder.matches(password, applicationUser.getPassword());
        return success
                ? this.onSuccessfullyLoginRequest(applicationUser.getUsername(), email)
                : new RequestResult(HttpStatus.BAD_REQUEST, FAILURE_INVALID_USERNAME_OR_PASSWORD);
    }

    @Override
    public RequestResult confirmRegistration(ConfirmUserRegistration confirmUserRegistration) {

        final String email = confirmUserRegistration.getEmail();

        /* Here we check if the confirmation message we got from front-end is correct with what we have saved in the database
           Correct as in:
           1) same email from where we have received the code
           2) same email code
           3) this validation happens before the code could have expired */
        final String sql = "SELECT email, email_code, email_code_exp_date" +
                " FROM user_table " +
                " WHERE email = ?" +
                " AND email_code = ?" +
                " AND email_code_exp_date > CURRENT_TIMESTAMP";
        try {
            boolean success = Optional.ofNullable(this.jdbcTemplate.queryForObject(sql,
                    new Object[]{email, confirmUserRegistration.getEmailCode()},
                    (resultSet, i) -> resultSet.getString("email").equals(email)
                            && resultSet.getString("email_code").equals(confirmUserRegistration.getEmailCode())))
                    .orElse(false);
            if (success) {
                this.clearEmailCode(email);
                return new RequestResult(HttpStatus.OK, SUCCESS_MSG);
            }
            return new RequestResult(HttpStatus.BAD_REQUEST, FAILURE_INCORRECT_EMAIL_CODE);
        } catch (DataAccessException e) {
            this.logger.warning("Failed to validate the email code for user " + confirmUserRegistration.getEmail());
            this.logger.warning(e.getMessage());
            return new RequestResult(HttpStatus.BAD_REQUEST, FAILURE_INCORRECT_EMAIL_CODE);
        }
    }

    @Override
    public RequestResult registerUser(String email, String username, String password) {
        if (this.checkEmailIsBlocked(email)) {
            this.logger.warning("Email: " + email + " did not validate his account");
            return new RequestResult(HttpStatus.BAD_REQUEST, FAILURE_EMAIL_CODE_NOT_VALIDATED);
        } else if (this.hasEmail(email)) {
            this.logger.warning("Email: " + email + " is already registered");
            return new RequestResult(HttpStatus.BAD_REQUEST, FAILURE_USER_ALREADY_EXISTS);
        } else if (this.isEmailInvalid(email)) {
            this.logger.warning("Email: " + email + " has an invalid format");
            return new RequestResult(HttpStatus.BAD_REQUEST, FAILURE_INVALID_EMAIL_FORMAT_MSG);
        }

        this.checkExpiredConfirmationEmailCode(email);

        // store the password as hashed
        final String hashedPassword = this.passwordEncoder.encode(password);

        // generate a code which the user has to send back to confirm his email
        final String emailCode = EmailUniqueCode.generateCode();

        final String sql = "INSERT INTO " +
                " user_table (email, username, password, is_admin, acc_creation_time, email_code, email_code_exp_date, email_is_blocked)" +
                " VALUES(?, ?, ?, ?, ?, ?, ?, true);";
        try {
            this.jdbcTemplate.update(sql,
                    email,
                    username,
                    hashedPassword,
                    false,
                    Instant.now().toEpochMilli(),
                    emailCode,
                    Timestamp.from(Instant.now().plusSeconds(this.codeExpiresIn))
            );
            // Send the code for the user to check in his email
            this.emailService.sendMultipartHtmlMessage(email, "no-reply",
                    this.emailService.registerMessageTemplate(emailCode));
        } catch (Exception e) {
            this.logger.warning("Couldn't register email: " + email);
            return new RequestResult(HttpStatus.BAD_REQUEST, FAILURE_INTERNAL_MSG);
        }
        return new RequestResult(HttpStatus.OK, SUCCESS_MSG);
    }

    @Override
    public Optional<ApplicationUser> getUserByEmail(String email) {
        final String sql = "SELECT " +
                " email   ," +
                " username," +
                " password," +
                " is_admin," +
                " last_added_post," +
                " acc_creation_time" +
                " FROM user_table WHERE username = ?";
        try {
            return Optional.ofNullable(this.jdbcTemplate.queryForObject(sql,
                    new Object[]{email},
                    (resultSet, i) -> new ApplicationUser(
                            resultSet.getString("email"),
                            resultSet.getString("username"),
                            resultSet.getString("password"),
                            true,
                            true,
                            true,
                            true,
                            USER.getGrantedAuthorities(),
                            resultSet.getDate("last_added_post"),
                            resultSet.getDate("acc_creation_time"))
            ));
        } catch (Exception e) {
            this.logger.warning(e.getMessage());
            return Optional.empty();
        }
    }

    @Override
    public Optional<ApplicationUser> getUserByUsername(String username) {
        final String sql = "SELECT" +
                " email   ," +
                " username," +
                " password," +
                " is_admin," +
                " last_added_post," +
                " acc_creation_time" +
                " FROM user_table WHERE username = ?";
        try {
            return Optional.ofNullable(this.jdbcTemplate.queryForObject(sql,
                    new Object[]{username},
                    (resultSet, i) -> new ApplicationUser(
                            resultSet.getString("email"),
                            resultSet.getString("username"),
                            resultSet.getString("password"),
                            true,
                            true,
                            true,
                            true,
                            USER.getGrantedAuthorities(),
                            resultSet.getDate("last_added_post"),
                            resultSet.getDate("acc_creation_time"))
            ));
        } catch (Exception e) {
            this.logger.warning(e.getMessage());
            return Optional.empty();
        }
    }

    @Override
    public RequestResult changePSWD(String emailCode, String email, String newPassword) {

        //First we have to check if the given email code is the right one and if it didn't expire.

        final String verifyEmailCodeSQL = "SELECT reset_password_code " +
                " FROM user_table " +
                " WHERE email = ?" +
                " AND reset_password_code_exp_date > CURRENT_TIMESTAMP" +
                " AND reset_password_code = ?";
        Optional<Integer> invalidCode = Optional.ofNullable(this.jdbcTemplate.queryForObject(verifyEmailCodeSQL, new Object[]{email, emailCode},
                Integer.class));
        if (invalidCode.isEmpty()) {
            // This can happen if:
            // 1) The code has expired
            // 2) The entered code doesn't match the one stored for this email
            this.cleanResetPswdCode(email);
            this.logger.warning("Change password code expired for email: " + email);
            return new RequestResult(HttpStatus.BAD_REQUEST, FAILURE_EXPIRED_RESET_PASSWORD_CODE);
        }

        this.cleanResetPswdCode(email);

        final String newEncryptedPSWD = this.passwordEncoder.encode(newPassword);
        final String sql = "UPDATE user_table SET password = ? WHERE email = ?;";
        return this.jdbcTemplate.update(sql, newEncryptedPSWD, email) > 0 ?
                new RequestResult(HttpStatus.OK, SUCCESS_MSG) :
                new RequestResult(HttpStatus.BAD_REQUEST, FAILURE_INTERNAL_MSG);
    }

    @Override
    public RequestResult deleteUser(String email, String password) {
        final RequestResult hasLogin = this.login(email, password, null);
        if (hasLogin.getStatus() != HttpStatus.OK) {
            return new RequestResult(HttpStatus.NOT_ACCEPTABLE, FAILURE_INVALID_USERNAME_OR_PASSWORD);
        }
        final String sql = "DELETE FROM user_table WHERE email = ?;";
        final boolean update = this.jdbcTemplate.update(sql, email) > 0;
        if (update) {
            return new RequestResult(HttpStatus.OK, SUCCESS_MSG);
        }
        return new RequestResult(HttpStatus.INTERNAL_SERVER_ERROR, FAILURE_INTERNAL_MSG);
    }

    @Override
    public RequestResult sendCodeToEmail(String email) {
        // First, we have to check if we actually have a email registered
        if (!this.hasEmail(email)) {
            return new RequestResult(HttpStatus.NOT_ACCEPTABLE, FAILURE_INVALID_EMAIL_MSG);
        }
        /* Send a randomly generated code to the server and store it in our database.
         * NOTE: the code will expire after @codeExpiresIn */
        final String code = EmailUniqueCode.generateCode();
        this.emailService.sendMultipartHtmlMessage(email, "no-reply",
                this.emailService.resetPasswordMessageTemplate(code));

        final String sql = "UPDATE user_table SET reset_password_code = ?, reset_password_code_exp_date = ? WHERE email = ?;";
        final boolean update = this.jdbcTemplate.update(sql, code, Timestamp.from(Instant.now().plusSeconds(this.codeExpiresIn)), email) > 0;
        if (update) {
            return new RequestResult(HttpStatus.OK, SUCCESS_MSG);
        }
        return new RequestResult(HttpStatus.BAD_REQUEST, FAILURE_INTERNAL_MSG);
    }

    /**
     * This functions clears the email_code and email_code_exp_date from user_table.
     * Used after the user has successfully registered or changed his password.
     * <p>
     * The user account won't be blocked any longer.
     *
     * @param email the email on which we want to apply the changes onto
     */
    private void clearEmailCode(String email) {
        // Now that we know the user has successfully registered we can clear some fields.
        final String sql = "UPDATE user_table SET email_code = NULL, email_code_exp_date = NULL, email_is_blocked=false WHERE email = ?";
        try {
            this.jdbcTemplate.update(sql, email);
        } catch (DataAccessException ex) {
            // This should never happen
            this.logger.warning(Arrays.toString(ex.getStackTrace()));
        }
    }

    /**
     * Blocks or unblocks a user based on his email address
     */
    private void blockEmail(String email, boolean block) {
        final String sql = "UPDATE user_table SET email_is_blocked = ? WHERE email = ?";
        try {
            this.jdbcTemplate.update(sql, block, email);
        } catch (DataAccessException ex) {
            // This should never happen
            this.logger.warning(Arrays.toString(ex.getStackTrace()));
        }
    }

    private boolean checkEmailIsBlocked(String email) {
        final String sql = "SELECT email, email_is_blocked FROM user_table WHERE email = ?";
        try {
            return Optional.ofNullable(this.jdbcTemplate.queryForObject(sql, new Object[]{email},
                    (resultSet, i) -> resultSet.getBoolean("email_is_blocked"))).orElse(false);
        } catch (Exception ignored) {
        }
        return false;
    }


    private boolean isEmailInvalid(String email) {
        return !EmailValidator.getInstance().isValid(email);
    }

    /**
     * Checks if the email validation code has expired and if so then block the account
     */
    private void checkExpiredConfirmationEmailCode(String email) {
        final String sql = "SELECT email, email_code_exp_date FROM user_table WHERE email = ? " +
                "AND email_code_exp_date > CURRENT_TIMESTAMP";
        try {
            Optional<Boolean> expired = Optional.ofNullable(this.jdbcTemplate.queryForObject(sql, new Object[]{email},
                    Boolean.class));
            if (expired.isPresent() && expired.get()) {
                this.blockEmail(email, true);
            }
        } catch (Exception ignored) {
        }
    }

    private void cleanResetPswdCode(String email) {
        final String cleanEmailCodeSQL = "UPDATE user_table SET reset_password_code_exp_date = NULL, reset_password_code = NULL WHERE email = ?;";
        this.jdbcTemplate.update(cleanEmailCodeSQL, email);
        this.logger.info("Cleaned the change password data for email: " + email);
    }

    /**
     * After a successful login we generate a JWT token
     *
     * @param email for which user we want to generate the JWT token
     * @return a success message which has the JWT token as the message, the front-end
     * will store this in it's local storage
     * and the username of the user
     */
    private RequestResult onSuccessfullyLoginRequest(String username, String email) {
        final Date expDate = new Date();
        expDate.setTime(expDate.getTime() + jwtKeyExpireIn);

        final String jwt = Jwts.builder()
                .setSubject(email)
                .setIssuedAt(new Date())
                .setExpiration(expDate)
                .signWith(SignatureAlgorithm.HS512, jwtKey)
                .compact();
        return new RequestResult(HttpStatus.OK, "{\"success\":1," +
                " \"msg\":\"" + jwt + "\"," +
                " \"username\":\"" + username + "\"}");
    }
}
