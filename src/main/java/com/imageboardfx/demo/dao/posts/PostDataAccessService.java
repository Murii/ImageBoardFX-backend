package com.imageboardfx.demo.dao.posts;

import com.imageboardfx.demo.pojo.Post;
import com.imageboardfx.demo.pojo.RequestResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collection;
import java.util.logging.Logger;

@Repository("postgres-post")
public class PostDataAccessService implements PostDAO {

    /**
     * Tells the system where to save the images for new posts
     */
    @Value("${posts.save-directory}")
    private String postSaveDirectory;

    private final JdbcTemplate jdbcTemplate;
    private final Logger logger = Logger.getLogger(PostDataAccessService.class.getName());

    @Autowired
    public PostDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public RequestResult newPost(Post post) {
        final String sql = "INSERT INTO posts " +
                "(name, author, dataType, imagePATH, description, tags, creationDate, isEdited) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
        try {
            this.jdbcTemplate.update(sql,
                    post.getTitle(), post.getAuthor(),
                    post.getDataType(), post.getImagePATH(),
                    post.getDescription(), Arrays.toString(post.getTags()),
                    post.getCreationDate(), false);
        } catch (Exception e) {
            this.logger.warning("Couldn't insert new post named: " + post.getTitle() + "; reason: \n" + e.getMessage());
            return new RequestResult(HttpStatus.INTERNAL_SERVER_ERROR, FAILURE_INTERNAL_MSG);
        }
        return new RequestResult(HttpStatus.OK, SUCCESS_MSG);
    }

    @Override
    public RequestResult updatePost(long id, Post newPost) {
        //TODO: do a check on the fields that need to be updated: eg do we need to update the description?
        final String sql = "UPDATE posts SET name = ?, description = ? WHERE id = ?;";
        try {
            boolean res = this.jdbcTemplate.update(sql, newPost.getTitle(), newPost.getDescription(), id) > 0;
            return res ? new RequestResult(HttpStatus.NOT_FOUND, FAILURE_MSG) : new RequestResult(HttpStatus.OK, SUCCESS_MSG);
        } catch (DataAccessException e) {
            this.logger.warning("Couldn't update post with id: " + id);
            return new RequestResult(HttpStatus.NOT_FOUND, FAILURE_NOT_FOUND_MSG);
        }
    }

    @Override
    public RequestResult getPost(long id) {
        final String sql = "SELECT * FROM posts WHERE id = ?;";
        try {
            final Post r = this.jdbcTemplate.queryForObject(sql,
                    new Object[]{id},
                    (resultSet, i) -> {
                        final Post post = new Post();
                        post.setId(resultSet.getLong("id"));
                        post.setTitle(resultSet.getString("name"));
                        post.setImagePATH(resultSet.getString("imagePATH"));
                        post.setDescription(resultSet.getString("description"));
                        post.setAuthor(resultSet.getString("author"));
                        post.setDataType(resultSet.getString("dataType"));
                        post.setCreationDate(resultSet.getLong("creationDate"));
                        post.setTags(resultSet.getString("tags").split(","));
                        try {
                            post.setImageData(extractBytes(post.getTitle(), post.getImagePATH()));
                        } catch (IOException ignored) {
                        }
                        return post;
                    }
            );
            return new RequestResult(HttpStatus.OK, r);
        } catch (DataAccessException e) {
            this.logger.warning("Couldn't get post with id: " + id);
            return new RequestResult(HttpStatus.NOT_FOUND, FAILURE_NOT_FOUND_MSG);
        }
    }

    @Override
    public RequestResult getPosts(long startIndex, long limit) {
        /*
         * Gets all the posts starting with "startIndex" and limit the result to "limit"
         */
        final String sql = "SELECT * FROM posts WHERE id >= " + startIndex + " LIMIT " + limit;
        try {
            Collection<Post> r = this.jdbcTemplate.query(sql, (resultSet, i) -> {
                        final Post post = new Post();
                        post.setId(resultSet.getLong("id"));
                        post.setTitle(resultSet.getString("name"));
                        post.setImagePATH(resultSet.getString("imagePATH"));
                        post.setDescription(resultSet.getString("description"));
                        post.setAuthor(resultSet.getString("author"));
                        post.setDataType(resultSet.getString("dataType"));
                        post.setCreationDate(resultSet.getLong("creationDate"));
                        post.setTags(resultSet.getString("tags").split(","));
                        try {
                            post.setImageData(extractBytes(post.getTitle(), post.getImagePATH()));
                        } catch (IOException ignored) {
                        }
                        return post;
                    }
            );
            if (r.isEmpty()) {
                return new RequestResult(HttpStatus.NOT_FOUND, FAILURE_NO_POSTS_MSG);
            }
            return new RequestResult(HttpStatus.OK, r);
        } catch (DataAccessException e) {
            this.logger.warning("Failed to fetch posts, limit: " + limit);
            return new RequestResult(HttpStatus.INTERNAL_SERVER_ERROR, null);
        }
    }

    @Override
    public RequestResult getNumberOfPost() {
        final String sql = "SELECT COUNT(*) FROM posts;";
        try {
            Integer resp = this.jdbcTemplate.queryForObject(sql, new Object[]{}, Integer.class);
            return new RequestResult(HttpStatus.OK, resp);
        } catch (Exception e) {
            this.logger.warning(e.getMessage());
        }
        return new RequestResult(HttpStatus.INTERNAL_SERVER_ERROR, 0);
    }

    /**
     * Gets from the local disk the required image and converts it into a base64 string
     */
    private String extractBytes(String postTitle, String imageName) throws IOException {
        File imgFile = new File(postSaveDirectory + postTitle + '-' + imageName);
        return javax.xml.bind.DatatypeConverter.printBase64Binary(Files.readAllBytes(imgFile.toPath()));
    }

    @Override
    public RequestResult deletePost(long id) {
        final String sql = "DELETE FROM posts WHERE id = ?;";
        try {
            this.jdbcTemplate.update(sql, id);
        } catch (DataAccessException e) {
            this.logger.warning("Couldn't delete post with id: " + id);
            return new RequestResult(HttpStatus.INTERNAL_SERVER_ERROR, FAILURE_INTERNAL_MSG);
        }
        return new RequestResult(HttpStatus.OK, SUCCESS_MSG);
    }


}
