package com.imageboardfx.demo.dao.posts;

import com.imageboardfx.demo.pojo.Post;
import com.imageboardfx.demo.pojo.RequestResult;

public interface PostDAO {

    String SUCCESS_MSG = "{\"success\":1}";
    String FAILURE_MSG = "{\"success\":0}";
    String FAILURE_DUPLICATE_POST_MSG = "{\"success\":0, \"msg\":\"Dublicated post name\"}";
    String FAILURE_INTERNAL_MSG = "{\"success\":0, \"msg\":\"Internal server error\"}";
    String FAILURE_NO_POSTS_MSG = "{\"success\":0, \"msg\":\"No posts\"}";
    String FAILURE_NOT_FOUND_MSG = "{\"success\":0, \"msg\":\"\"}";

    /**
     * Add a new post into the database
     */
    RequestResult newPost(Post post);

    /**
     * Modified a post with new data
     */
    RequestResult updatePost(long id, Post newPost);

    /**
     * Gets a specific post from the database
     */
    RequestResult getPost(long id);

    /**
     * Starting with "startIndex" we'll get all the posts within the "limit"
     */
    RequestResult getPosts(long startIndex, long limit);

    RequestResult getNumberOfPost();

    /**
     * Delete the wanted post
     */
    RequestResult deletePost(long id);
}
