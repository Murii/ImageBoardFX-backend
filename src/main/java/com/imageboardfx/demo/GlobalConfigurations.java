package com.imageboardfx.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * This class is used to read properties fields from "application.yaml"
 *
 * @author muresan vlad mihail
 */
@Component
public class GlobalConfigurations {
//
//    @Value("${app.config.ip}")
//    private String ip;
//
//    @Value("${app.config.port}")
//    private int port;
//
//    public String getIp() {
//        return ip;
//    }
//
//    public int getPort() {
//        return port;
//    }
}
