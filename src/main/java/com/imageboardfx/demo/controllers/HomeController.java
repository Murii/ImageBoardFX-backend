package com.imageboardfx.demo.controllers;

import com.imageboardfx.demo.pojo.Post;
import com.imageboardfx.demo.pojo.RequestResult;
import com.imageboardfx.demo.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.TemporalField;
import java.util.*;
import java.util.logging.Logger;

@RequestMapping(value = "/home")
@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class HomeController {

    private final Logger logger = Logger.getLogger(HomeController.class.getName());

    /**
     * Tells the system where to save the images for new posts
     */
    @Value("${posts.save-directory}")
    private String postSaveDirectory;

    @Value("${posts.image-width}")
    private int postImageWidth;

    @Value("${posts.image-height}")
    private int postImageHeight;

    @Value("${posts.image-save-format}")
    private String postImageSaveFormat;

    @Autowired
    private PostService postService;

    @PostConstruct
    public void init() {
        try {
            File saveDirectory = new File(postSaveDirectory);
            /**
             * Check to see if we have a directory where we can save the
             * posts' images and if not create it (if we're allowed)
             */
            if (!saveDirectory.exists() || saveDirectory.isDirectory() || !saveDirectory.mkdir()) {
                this.logger.warning("Couldn't create directory: " + postSaveDirectory + " for saving posts images!");
            }
        } catch (Exception se) {
            this.logger.warning(se.getMessage());
        }
    }

    @GetMapping("/getPosts/{startIndex}/{limit}")
    public ResponseEntity<Collection<Post>> getPosts(@Valid @NonNull @PathVariable long startIndex,
                                                     @Valid @NonNull @PathVariable long limit) {
        final RequestResult r = this.postService.getPosts(startIndex, limit);
        return new ResponseEntity<Collection<Post>>((Collection<Post>)r.getRespObject(), r.getStatus());
    }

    @GetMapping(path = "/getPost/{id}")
    public ResponseEntity<Post> getPost(@NotBlank @PathVariable("id") long id) {
        RequestResult r = this.postService.getPost(id);
        return new ResponseEntity<Post>((Post) r.getRespObject(), r.getStatus());
    }

    @GetMapping(path = "/getNumberOfPost")
    public ResponseEntity<Integer> getNumberOfPost() {
        RequestResult r = this.postService.getNumberOfPost();
        return new ResponseEntity<Integer>((Integer) r.getRespObject(), r.getStatus());
    }

    @PostMapping(value = "/newPost", consumes = {"multipart/form-data"})
    public ResponseEntity<String> newPost(@NotBlank @RequestPart("properties") Map<String, Object> properties,
                                          @Valid @NotNull @NotBlank @RequestPart("imageFile") MultipartFile image) throws IOException, ParseException {
        Post newPost = new Post();
        try {
            newPost.setTitle(properties.get("title").toString());
            newPost.setImagePATH(image.getOriginalFilename());
            newPost.setDescription(properties.get("description").toString());
            newPost.setAuthor(properties.get("author").toString());
            newPost.setTags(properties.get("tags").toString().split(","));
        } catch (NullPointerException ex) {
            this.logger.warning("Expected field, " + ex.getMessage());
            return new ResponseEntity<String>("Invalid request format", HttpStatus.NOT_FOUND);
        }

        newPost.setCreationDate(Instant.now().toEpochMilli());
        newPost.setDataType(image.getContentType());

        RequestResult r = this.postService.newPost(newPost);
        if (r.getStatus() == HttpStatus.OK) {
            this.saveImageToDisk(image.getBytes(), newPost.getTitle(), newPost.getImagePATH());
        }
        return new ResponseEntity<String>(r.getMessage(), r.getStatus());
    }

    @PutMapping("/updatePost/{id}")
    public ResponseEntity updatePost(@NotBlank @PathVariable("id") String name,
                                     @Valid @NonNull @RequestBody Map<String, Object> body) {
        return null;
    }

    @DeleteMapping("/deletePost/{id}")
    public ResponseEntity deletePost(@NotBlank @PathVariable("id") long id) {
        RequestResult r = this.postService.deletePost(id);
        return new ResponseEntity<String>(r.getMessage(), r.getStatus());
    }

    private void saveImageToDisk(byte[] imageByteData, String postTitle, String postOriginalFilename ) throws IOException {
        ByteArrayInputStream bais = null;
        ByteArrayOutputStream baos = null;
        try {
            bais = new ByteArrayInputStream(imageByteData);
            BufferedImage bufferedImage = ImageIO.read(bais);
            BufferedImage obi = this.resizeImage(bufferedImage, postImageWidth, postImageHeight);
            baos = new ByteArrayOutputStream();
            ImageIO.write(obi, postImageSaveFormat, baos);

            /**
             * The final image will something like:
             * ./path-to-all-post-images/post-title-image-name.format
             */
            final String postName = postSaveDirectory + postTitle + '-' + postOriginalFilename;
            try (FileOutputStream saveImage = new FileOutputStream(postName)) {
                saveImage.write(baos.toByteArray());
            }
        } catch (Exception e) {
            this.logger.warning(e.getMessage());
            if (bais != null) {
                bais.close();
            }
            if (baos != null) {
                baos.close();
            }
        }
    }

    /**
     * Used to scale a loaded buffered image to desired width and height
     */
    private BufferedImage resizeImage(BufferedImage originalImage, int targetWidth, int targetHeight) throws IOException {
        // scale only if needed
        if (originalImage.getWidth() >= targetWidth && originalImage.getHeight() >= targetHeight) {
            return originalImage;
        }
        Image resultingImage = originalImage.getScaledInstance(targetWidth, targetHeight, Image.SCALE_DEFAULT);
        BufferedImage outputImage = new BufferedImage(targetWidth, targetHeight, BufferedImage.TYPE_INT_RGB);
        outputImage.getGraphics().drawImage(resultingImage, 0, 0, null);
        return outputImage;
    }
}
