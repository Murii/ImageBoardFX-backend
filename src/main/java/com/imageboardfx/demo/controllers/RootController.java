package com.imageboardfx.demo.controllers;

import com.imageboardfx.demo.pojo.ConfirmUserRegistration;
import com.imageboardfx.demo.pojo.RequestResult;
import com.imageboardfx.demo.services.ApplicationUserService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.Map;

@RequestMapping(value = "/")
@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class RootController {

    private final ApplicationUserService applicationUserService;

    public RootController(ApplicationUserService applicationUserService) {
        this.applicationUserService = applicationUserService;
    }

    @PostMapping(value = "/confirm-registration", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> confirmRegistration(@Valid @NotBlank @RequestBody ConfirmUserRegistration confirmation) {
        final RequestResult confirmRegistration = applicationUserService.confirmRegistration(confirmation);
        return new ResponseEntity<String>(confirmRegistration.getMessage(), confirmRegistration.getStatus());
    }

    @PutMapping(value = "/register", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> register(@NotBlank @RequestBody Map<String, String> body) {
        final RequestResult register = applicationUserService.register(body.get("email"), body.get("username"), body.get("password"));
        return new ResponseEntity<String>(register.getMessage(), register.getStatus());
    }

    @PostMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> login(@NotBlank @RequestBody Map<String, String> body) {
        final RequestResult login = applicationUserService.login(body.get("email"), body.get("password"), body.get("jwtKey"));
        return new ResponseEntity<String>(login.getMessage(), login.getStatus());
    }

    @PostMapping(value = "/pre-changepswd", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> preChangePswd(@NotBlank @RequestBody Map<String, String> body) {
        final RequestResult sendCodeToEmail = applicationUserService.sendCodeToEmail(body.get("email"));
        return new ResponseEntity<String>(sendCodeToEmail.getMessage(), sendCodeToEmail.getStatus());
    }

    @PostMapping(value = "/changepswd", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> changePSWD(@NotBlank @RequestBody Map<String, String> body) {
        final RequestResult changePSWD = applicationUserService.changePSWD(body.get("email_code"), body.get("email"),
                body.get("new_password"));
        return new ResponseEntity<String>(changePSWD.getMessage(), changePSWD.getStatus());
    }

    @DeleteMapping(value = "/delete/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> delete(@Valid @NotBlank @RequestBody Map<String, String> body) {
        final RequestResult deleted = applicationUserService.deleteUser(body.get("email"), body.get("password"));
        return new ResponseEntity<String>(deleted.getMessage(), deleted.getStatus());
    }
}
