package com.imageboardfx.demo.security;

import java.util.Random;

/**
 * Used to generate 6 digits code for when
 * the user registers or resets the password
 */
public class EmailUniqueCode {

    /**
     * @return a 6 digit random string
     */
    public static String generateCode() {
        Random random = new Random();
        String code = String.valueOf(random.nextInt(9));

        for (int i = 0; i < 5; i++) {
            code += String.valueOf(random.nextInt(9));
        }

        return code;
    }

}
