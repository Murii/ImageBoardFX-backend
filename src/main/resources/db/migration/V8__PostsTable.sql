-- the author (account) of the post
ALTER TABLE posts ADD COLUMN author VARCHAR;
-- dataType can be: data/png, data/jpg; useful metadata for frontend to render
ALTER TABLE posts ADD COLUMN dataType VARCHAR;