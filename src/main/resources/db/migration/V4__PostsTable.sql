ALTER TABLE user_table ADD COLUMN reset_password_code VARCHAR(6);
ALTER TABLE user_table ADD COLUMN reset_password_code_exp_date TIMESTAMP;