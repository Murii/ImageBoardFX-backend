CREATE TABLE IF NOT EXISTS user_table (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    email    VARCHAR(32) NOT NULL,
    username VARCHAR(20) NOT NULL,
    password VARCHAR(128) NOT NULL,
    is_admin BOOLEAN,
    last_added_post BIGINT,
    acc_creation_time BIGINT
);