ALTER TABLE user_table ADD COLUMN email_code VARCHAR(6);
ALTER TABLE user_table ADD COLUMN email_code_exp_date timestamp;
ALTER TABLE user_table ADD COLUMN email_is_blocked BOOLEAN;
