-- tells if a post was edited or not
ALTER TABLE posts ADD COLUMN isEdited BOOLEAN;