# ImageBoardFX-backend

ImageBoardFX is a lightweight application used to host images in a public space. This is the back-end part of the application
which deals with the REST API calls and database management and more.

# Setup 
## Java
Be sure to have java *11* installed

## Postgres 
Postgres is ran with docker. After installing postgres: 
1) `sudo docker run --name postgres-spring -e POSTGRES_PASSWORD=password1 -d -p 5432:5432 postgres:alpine`
2) `docker ps` you should see `postgres-spring` running. If not then run `docker start postgres-spring`
3) `docker exec -it <ID> (see docker ps) bin/bash`
4) `psql -U postgres`
5) `CREATE DATABASE imageboardfx;`
6) `\l` should list `imageboardfx`
7) exit the container


*Note*: It's important to be able to run Docker without needing sudo access.
1) `sudo groupadd docker`
2) `sudo gpasswd -a $USER docker`
3) `newgrp docker`
4) logout from your current linux session and come back
5) docker ps should work without root now
Read more:
https://askubuntu.com/questions/477551/how-can-i-use-docker-without-sudo

